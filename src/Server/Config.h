#pragma once

#define CURL_STATICLIB
#define MAX_SIZE_MESSAGE 1024

enum transport_type
{
	Socket,
	Pipe
};

void set_config(std::string& ip_adr, std::string& port);