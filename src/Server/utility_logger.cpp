#include "stdafx.h"
#include "Utility.h"
#include <time.h>

long get_file_size(std::string path)
{
	FILE *pFile = fopen(path.data(), "rb");
	long nFileLen = 0;
	if (pFile)
	{
		fseek(pFile, 0, SEEK_END);
		nFileLen = ftell(pFile);
		fclose(pFile);
	}

	return nFileLen;
}

const std::string currentDateTime()
{
	time_t now = time(0);
	struct tm tstruct;
	char buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	for (int index = 0; index < strlen(buf); ++index)
	{
		if (buf[index] == ':')
		{
			buf[index] = '~';
		}
	}

	return buf;
}

void set_min_level_msg(std::string level_msg, int& min)
{
	if (level_msg == "DEBUG")
		min = 0;
	if (level_msg == "INFO")
		min = 1;
	if (level_msg == "WARNING")
		min = 2;
	if (level_msg == "ERROR")
		min = 3;
}

void set_max_level_msg(std::string level_msg, int& max)
{
	if (level_msg == "DEBUG")
		max = 0;
	if (level_msg == "INFO")
		max = 1;
	if (level_msg == "WARNING")
		max = 2;
	if (level_msg == "ERROR")
		max = 3;
}

