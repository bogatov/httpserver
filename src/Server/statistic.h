
//��
//����� ����� ����������� ��������;
//����� ����������� ��������;
//����� ����� ���������� � ���������� ������;
//����� ������, ���������� �������� � ��������� IP - �������;
//����� �������� ����������� online.
#pragma once
#include <time.h>
#include <map>
#include <set>
#include <algorithm>
#include "Package.h"

//using namespace std;

class Statistic
{
public:
	Statistic();

	//��������� ���������� �� ������� package � ������ statistic
	int translator(package pack);

	//���������� �������� ����� ��� clientUpdate �� ������ ������� 
	int clientUpdateFlag(std::string request);

	//��������� ������ �������:
	//		name - ��� �������
	//		data - ����� ���������� ��� ���������� ������
	//		flag - ���� � ����������� �� �������� �������� ����������� ������:
	//			0 - ������ ������� ��������� � ������ ����������,
	//			1 - c����� ������� ���������, � ������ ����������,
	//			2 - ������ ������� ���������, � ������������ ����������,
	//			3 - c����� ������� ���������, � ������������ ����������
	int clientUpdate(int ip, unsigned long long int data, int flag);

	//���������� ����� �������� �� ��� �����
	int getMaxCount(); 

	//���������� ��������� ����� ���������� ������� ������
	unsigned long long int getDataOut(); 

	//���������� ��������� ����� ���������� ������� ������
	unsigned long long int getDataIn();

	// ���������� ������� ����� ��������
	int getCount();

	 // ���������� ������ ���� ��������
	std::set<std::string> getClientsIp();

	//���������� ����� ������ ���������� �������
	unsigned long long int clientDataOut(int ip);

	//���������� ����� ������ ���������� �������
	unsigned long long int clientDataIn(int ip);

	//���������� ����� ����������� ������� � ��������
	int clientLifetime(int ip); 

private:

	//��������� ����� ���������� ������� ������
	unsigned long long int dataOut;

	//��������� ����� ���������� ������� ������
	unsigned long long int dataIn;

	class client
	{
	public:
		client();
		friend Statistic;

	private:

		//���� ��������� ����������
		bool keepAliveFlag;

		// ����� ��������
		int creationTime;

		//����� ���������� ������� ������
		unsigned long long int dataOut;

		//����� ���������� ������� ������
		unsigned long long int dataIn;
	};
		
	//������ �������� ������������ � ������� ������
	std::map<int, client> clientbase;
};

