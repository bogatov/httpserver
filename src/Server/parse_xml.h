#pragma once

#include <iostream>
#include "tinyxml2.h"

class parse_xml
{
public:
	parse_xml();
	~parse_xml() {};

	//��������� ������������ ������� ���������
	std::string get_max_level_msg();

	//��������� ����������� ������� ���������
	std::string get_min_level_msg();

	//��������� ������������ ������ ����� ����� 
	std::string get_max_size_file_log();

	//��������� ������ ����� �����
	std::string get_way_write_log();

	//�������� ���� �� ����� �����
	std::string get_path_file_log();

private:
	tinyxml2::XMLDocument doc;
};