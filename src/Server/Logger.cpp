#include "stdafx.h"
#include "logger.h"
#include <time.h>
#include "Utility.h"
#include <io.h>
#include "parse_xml.h"
#include "utility_logger.h"

logger::logger(int min_level_msg, int max_level_msg, int max_size_file_log, std::string way_write_log, std::string path)
{
	_min_level_msg = min_level_msg;
	_max_level_msg = max_level_msg;
	_max_size_file_log = max_size_file_log;
	_current_size_file_log = 0;
	_way_write_log = way_write_log;
	_path = path;

	if (way_write_log == "file")
		m_stream.open(_path, std::ios::app);
}

void logger::settings_file_config_xml()
{
	//������� ������ ParseXml. ����� ���� �������� �����. ��������� ������ ������ � ����� config.xml 
	parse_xml parse;
	std::string max_level_msg = parse.get_max_level_msg();
	std::string min_level_msg = parse.get_min_level_msg();
	std::string max_size_file_log = parse.get_max_size_file_log();
	std::string way_write_log = parse.get_way_write_log();

	if (way_write_log == "file")
	{
		_path = parse.get_path_file_log();
		m_stream.open(_path, std::ios::app);
	}

	_way_write_log = way_write_log;

	//��������� ������������ ������ ���������
	set_min_level_msg(min_level_msg, _min_level_msg);

	//��������� ������������� ������ ���������
	set_max_level_msg(max_level_msg, _max_level_msg);

	//��������� ������������� ������� ����� �����
	_max_size_file_log = atoi(max_size_file_log.c_str());
	_current_size_file_log = 0;
}

logger::logger()
{
	settings_file_config_xml();
}

logger::~logger()
{
	m_stream.close();
}

void logger::_DEBUG_(std::string msg, bool name_method, bool id_stream)
{
	//���� ���������� ������� ��������� ���� DEBAG (=0) �� ��� ��������� �����������
	if (_min_level_msg == 0)
	{
		std::string data = currentDateTime();
		std::string log = data + "[" + "__DEBUG__" + "]" + msg;

		if (name_method != 0)
			log += " name_method : __DEBUG__ ";
		
		write_msg(log);
	}
}

void logger::_INFO_(std::string msg, bool name_method, bool id_stream)
{
	if (_min_level_msg <= 1)
	{
		std::string data = currentDateTime();
		std::string log = data + "[" + "__INFO__" + "]" + msg;

		if (name_method != 0)
			log += " name_method : __INFO__ ";

		write_msg(log);
	}
}

void logger::_WARNING_(std::string msg, bool name_method, bool id_stream)
{
	if (_min_level_msg <= 2)
	{
		std::string data = currentDateTime();
		std::string log = data + "[" + "__WARNING__" + "]" + msg;

		if (name_method != 0)
			log += " name_method : __WARNING__ ";
		write_msg(log);
	}
}

void logger::_ERROR_(std::string msg, bool name_method, bool id_stream)
{
	if (_min_level_msg <= 3)
	{
		std::string data = currentDateTime();
		std::string log = data + "[" + "__ERROR__" + "]" + msg;

		if (name_method != 0)
			log += " name_method : __ERROR__ ";

		write_msg(log);
	}
}

void logger::check_overflow_log_file()
{
	if (get_file_size(_path) >= _max_size_file_log)
	{
		std::string data = currentDateTime();
		std::string new_file = _path + "(" + data + ")" + ".txt";
		m_stream.close();
		m_stream.open(new_file, std::ios_base::app);
		_path = new_file;
	}
}

void logger::write_msg(std::string log)
{
	if (_way_write_log == "file")
	{
		m_stream << log.data() << std::endl;
		check_overflow_log_file();
	}

	if (_way_write_log == "console")
		std::cout << log.data() << std::endl;
}



