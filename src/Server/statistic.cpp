#include "stdafx.h"
#include "statistic.h"


Statistic::Statistic()
{
	 dataIn = 0;
	 dataOut = 0;
	clientbase = std::map<int, client>();
}

int Statistic::translator(package pack)
{
	int flag = clientUpdateFlag(pack.request);
	unsigned long long int data = pack.request.size();
	clientUpdate(pack.int_ip_client, data, flag);
	return 0;
}

int Statistic::clientUpdateFlag(std::string request)
{
	if (strstr(request.c_str(), "Host:") != nullptr) {
		if (strstr(request.c_str(), "Connection: keep-alive"))
			return 2;
		else
			return 0;
	}
	if (strstr(request.c_str(), "Connection: keep-alive"))
		return 3;
	return 1;
}

int Statistic::clientUpdate(int ip, unsigned long long int data, int flag)
{
	if (data < 0 || ip == NULL)
		return 1;

	switch (flag) {
	case 0:
		clientbase[ip].keepAliveFlag = false;
		clientbase[ip].dataOut += data;
		dataOut += data;
		break;
	case 1:
		clientbase[ip].keepAliveFlag = false;
		clientbase[ip].dataIn += data;
		dataIn += data;
		break;
	case 2:
		clientbase[ip].keepAliveFlag = true;
		clientbase[ip].dataOut += data;
		dataOut += data;
		break;
	case 3:
		clientbase[ip].keepAliveFlag = true;
		clientbase[ip].dataIn += data;
		dataIn += data;
		break;
	}

	return 0;
}

int Statistic::getMaxCount()
{
	return clientbase.size();
}

unsigned long long int Statistic::getDataOut()
{
	return dataOut;
}

unsigned long long int Statistic::getDataIn()
{
	return dataIn;
}

int Statistic::getCount()
{
	int count = 0;
	for_each(clientbase.begin(), clientbase.end(), [&](const auto &elem) {
		if (elem.second.keepAliveFlag == 1)
			count++;
	});
	return count;
}

std::set<std::string> Statistic::getClientsIp()
{
	std::string name = std::string();
	char buff[4];
	std::set<std::string> clientNames;
	for_each(clientbase.begin(), clientbase.end(), [&](const auto &elem) {
		name = "";
		for (size_t i = 0; i != 4; ++i) {
			_itoa_s((elem.first >> (24 - 8 * i)) & 255, buff, 10);
			name += buff + std::string(".");
		}
		*(name.end() - 1) = '\0';
		clientNames.insert(name);
	});
	return clientNames;
}

unsigned long long int Statistic::clientDataOut(int ip)
{
	return clientbase[ip].dataOut;
}

unsigned long long int Statistic::clientDataIn(int ip)
{
	return clientbase[ip].dataIn;
}

 int Statistic::clientLifetime(int ip)
{
	return (clock() - clientbase[ip].creationTime)/CLOCKS_PER_SEC;
}

Statistic::client::client()
{
	keepAliveFlag == false;
	creationTime = clock();
	dataOut = 0;
	dataIn = 0;
}
