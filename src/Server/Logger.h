#pragma once

#include <iostream>
#include <fstream>
#include <thread>

class logger
{
public:
	logger();
	~logger();
	logger(int min_level_msg, int max_level_msg, int max_size_file_log, std::string way_write_log, std::string path);
	
	//������ ��������� ������ debag
	void _DEBUG_(std::string log, bool name_method =	0, bool id_stream = 0);
	
	//������ ��������� ������ info
	void _INFO_(std::string log, bool name_method = 0, bool id_stream = 0);

	//������ ��������� ������ warning
	void _WARNING_(std::string log, bool name_method = 0, bool id_stream = 0);

	//������ ��������� ������ error
	void _ERROR_(std::string log, bool name_method = 0, bool id_stream = 0);
	int get_current_size() { return _current_size_file_log; };

	//������� ����� ���� ����� � ������ ������������ ���������
	void check_overflow_log_file();
	void write_msg(std::string log);

	//��������� ������� ����� ���� xml
	void settings_file_config_xml();

private:
	std::ofstream m_stream;

	//����������� �� ����� xml
	// _DEBUG_ = 0
	// _INFO_ = 1
	// _WARNING_ = 2
	// _ERROR_ = 3
	int _min_level_msg;
	int _max_level_msg;
	int _max_size_file_log;
	int _current_size_file_log;
	std::string _way_write_log;
	std::string _path;
};
