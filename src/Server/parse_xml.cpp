#include "stdafx.h"
#include "parse_xml.h"

parse_xml::parse_xml()
{
	doc.LoadFile("config.xml");
}

std::string parse_xml::get_max_level_msg()
{
	const char* title = doc.FirstChildElement("rules")->FirstChildElement("logger")->Attribute("maxlevel");
	std::string max_level_msg = title;
	
	return max_level_msg;
}

std::string parse_xml::get_min_level_msg()
{
	const char* title = doc.FirstChildElement("rules")->LastChildElement("logger")->Attribute("minlevel");
	std::string min_level_msg = title;

	return min_level_msg;
}

std::string parse_xml::get_max_size_file_log()
{
	const char* title = doc.FirstChildElement("max_size_file_log")->GetText();
	std::string max_size_file_log = title;

	return max_size_file_log;
}

std::string parse_xml::get_way_write_log()
{
	const char* way_write_log = doc.FirstChildElement("output")->Attribute("type");
	std::string way = way_write_log;
	
	return way;
}

std::string parse_xml::get_path_file_log()
{
	const char* path_file_log = doc.FirstChildElement("output")->Attribute("name");
	std::string path = path_file_log;

	return path;
}
