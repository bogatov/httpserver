#pragma once

#include "stdafx.h"
#include "utility.h"

std::string get_url_and_port(std::string http_req)
{
	const auto pos_host = http_req.find("Host");
	std::string url_and_port;

	if (pos_host == std::string::npos)
		return url_and_port;

	for (auto i = pos_host + 6; http_req.at(i) != '\0' && http_req.at(i) != '\r' && http_req.at(i) != ' ' && http_req.at(i) != '\n' && http_req.at(i) != ':'; ++i)
		url_and_port.push_back(http_req.at(i));


	return url_and_port;
}