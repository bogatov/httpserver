#include "stdafx.h"
#include "config.h"
#include "logger.h"
#include "parse_xml.h"

void set_config(std::string& ip_adr, std::string& port)
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile("config.xml");
	port = doc.FirstChildElement("port")->GetText();
	ip_adr = doc.FirstChildElement("ip_addr")->GetText();
}