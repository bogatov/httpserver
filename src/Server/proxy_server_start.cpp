#include "stdafx.h"
#include "socket_transport.h"
#include "interface_transport.h"
#include <clocale>
#include <sstream>
#include <iostream>
#include <winsock2.h>
#include <thread>
#include <vector>
#include "utility.h"
#include "proxy.h"
#include "config.h"
#include "package.h"

sockaddr_in client_info{};


void send_answer(proxy proxy)
{
	int total_bytes_client_from_server = proxy.send_answer_to_client();
}

void client_server_work(SOCKET client_socket, package package_client)
{
	proxy proxy(client_socket);
	proxy.get_request_from_client();
	package_client.set_total_bytes_in_request_client(proxy.get_count_byets());
	package_client.set_request(proxy.get_request());
	std::string destination_address = proxy.create_server();
	package_client.set_destination_address(destination_address);
	proxy.send_request_server();
	std::thread thr3(send_answer, proxy);
	thr3.join();
}

void proxy_server_start()
{
	//������� ������ ������ Socket
	socket_transport socket(IP_ADR, PORT);

	//����������� ����� �� ������������� 
	socket.start_connection_client();

	//��������� ��������� �����
	SOCKET listen_socket = socket.get_socket();

	while (true)
	{
		socklen_t size_client_info = sizeof(client_info);

		//�������� ���������� �������
		SOCKET client_socket = accept(listen_socket, (sockaddr*)&client_info, &size_client_info);
		package package_client(client_socket);
		package_client.set_int_ip_client(client_info.sin_addr.S_un.S_addr);
		std::thread thr(client_server_work, client_socket, package_client);
		thr.join();
	}
}