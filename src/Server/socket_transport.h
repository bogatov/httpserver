#pragma once

#include "interface_transport.h"
#include "Package.h"
#include <iostream>
#include <thread>

//���������� ����������
class socket_transport : public transport
{
public:
	socket_transport() {};
	socket_transport(PCSTR ip_adress, PCSTR port, SOCKET client_socket);
	~socket_transport() { close(); freeaddrinfo(_result);};

	//������������� ����� �� ���������
	bool start_connection_client() override;

	//������������� ����� ��� ����������� � ���������� �������
	bool start_connection_server() override;

	//�������� ������ �� �������
	std::string get_request_from_client() override;

	//�������� ����� �� ������� � ��������� ��� �������
	std::string send_answer_to_client() override;

	//��������� ������ �� ������
	int send_request_server() override;

	//���������� ������ �� ������/����� ��������
	int send_request(int len, int flag) override;

	//��������� ���������� ����� ���� ��� ������ � ������ ���������� �������
	void close() override;
	void* get_transport() override;
	void setting_transport(void* arg) override;

private:
	SOCKET _socket;
	struct addrinfo _hints, *_result;

	//�������������� ������
	SOCKET _client_socket;

	//����� ��� �������� ������ �� ������
	SOCKET _server_socket;

	struct sockaddr_in my_addr;

	//������ �������
	std::string _request;
	std::shared_ptr<char> _server_answer;

	//���-�� ���� ���������� �������� � �������
	int _count_bytes;
};

