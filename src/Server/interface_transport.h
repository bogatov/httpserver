#pragma once

#include <winsock2.h>
#pragma comment(lib, "Ws2_32.lib")

class transport
{
public:

	//настраивается связь для обмена данными с клиентом
	virtual bool start_connection_client() = 0;

	//настраивается связь для обмена данными с удаленным сервером
	virtual bool start_connection_server() = 0;

	//получить запрос от клиента
	virtual std::string get_request_from_client() = 0;

	//получить ответ от сервера и отправить его клиенту
	virtual std::string send_answer_to_client() = 0;

	//отправить запрос на сервер
	virtual int send_request_server() = 0;

	//отправляем запрос на сервер/ответ браузеру
	virtual int send_request(int len, int flag) = 0;

	//разрывем соединение
	virtual void close() = 0;
	virtual void setting_transport(void* arg) = 0;
	virtual void* get_transport() = 0;
};











