﻿#include "stdafx.h"
#include "interface_transport.h"
#include "iostream"
#include <WS2tcpip.h>
#include "socket_transport.h"
#include <thread>
#include "Package.h"
#include "Config.h"
#include "Utility.h"
#include "logger.h"
#include "mutex"
#include "get_transport.h"

#define _BUF_SIZE 100000
logger* my_logger = new logger();
std::mutex mtx;

socket_transport::socket_transport(PCSTR ip_adress, PCSTR port, SOCKET client_socket)
{
	WSADATA WsaData;
	WSAStartup(0x0101, &WsaData);
	ZeroMemory(&_hints, sizeof &_hints);
	_hints.ai_family = AF_UNSPEC;
	_hints.ai_socktype = SOCK_STREAM;
	_hints.ai_protocol = IPPROTO_TCP;
	getaddrinfo(ip_adress, port, &_hints, &_result);

	//создаем сокет
	_socket = socket(_result->ai_family, _result->ai_socktype, _result->ai_protocol);
	_client_socket = client_socket;
	_server_socket = NULL;
	_request = "";
	_count_bytes = 0;
	_server_answer = std::shared_ptr<char>(new char[_BUF_SIZE]);
}

bool socket_transport::start_connection_client()
{
	const int err_bind = bind(_socket, _result->ai_addr, _result->ai_addrlen);

	if (err_bind == -1)
	{
		my_logger->_ERROR_("error bind socket", TRUE);

		return FALSE;
	}
	
	const int err_listen = listen(_socket, SOMAXCONN);

	if (err_listen == -1)
	{
		my_logger->_ERROR_("error listen socket", TRUE);

		return FALSE;
	}
	
	return TRUE;
}

bool socket_transport::start_connection_server()
{
	//соединяемся с сервером
	int success_connection = connect(_socket, _result->ai_addr, _result->ai_addrlen);

	if (success_connection != 0)
	{
		my_logger->_ERROR_("error connect socket", TRUE);
	}

	return success_connection == 0 ? TRUE : FALSE;
}

std::string socket_transport::get_request_from_client()
{
	unsigned long long  lenght = 0;

	std::shared_ptr<char> buf(new char[_BUF_SIZE]);

	do
	{
		//кол-во считанных байт
		lenght = recv(_client_socket, buf.get(), _BUF_SIZE, NULL);

		if (lenght == -1)
		{
			my_logger->_ERROR_("request read unsuccessfully", TRUE);
		}

		_count_bytes += lenght;
		buf.get()[lenght] = '\0';
		_request.append(buf.get());

	} while (lenght == _BUF_SIZE);
	
	my_logger->_INFO_("сlient request read successfully!", TRUE);

	return _request;
}

std::string socket_transport::send_answer_to_client()
{
	int lenght_server_answer = 0;

	do
	{
		//количетсво считанных байт
		lenght_server_answer = recv(_server_socket, _server_answer.get(), _BUF_SIZE, NULL);

		if (lenght_server_answer == 0)
		{
			my_logger->_WARNING_("recv from server_socket == 0", TRUE);
		}
		
		if (send_request(lenght_server_answer, 0) == -1)
		{
			my_logger->_ERROR_("error send request to client", TRUE);
		}
	
	} while (lenght_server_answer == _BUF_SIZE);

	std::string answer = _server_answer.get();
	
	if (answer.find("200 OK"))
	{
		return answer;
	}

	return nullptr;
}

int socket_transport::send_request(int len, int flag)
{
	int total_bytes = 0;
	int bytesleft = len;
	int count_bytes_send = 0;

	while (total_bytes < len)
	{
		if (flag == 0)
		{
			count_bytes_send = send(_client_socket, _server_answer.get() + total_bytes, bytesleft, 0);
			
			if (count_bytes_send == -1 || count_bytes_send == 0)
			{
				my_logger->_ERROR_("send request to client unsuccessfully", TRUE);

				return 0;
			}
			else
			{
				my_logger->_INFO_("send request to client successfully!", TRUE);
			}
		}
		else
		{
			count_bytes_send = send(_server_socket, _request.c_str() + total_bytes, bytesleft, 0);
			
			if (count_bytes_send == -1 || count_bytes_send == 0)
			{
				my_logger->_ERROR_("error send_request _server_socket", TRUE);

				return 0;
			}
			else
			{
				my_logger->_INFO_("send_request to server successfully!", TRUE);
			}
		}

		total_bytes += count_bytes_send;
		bytesleft -= count_bytes_send;
	}

	len = total_bytes;
	
	return total_bytes;
}

void socket_transport::close()
{
	closesocket(_client_socket);
	closesocket(_server_socket);
}

void* socket_transport::get_transport()
{
	return (void*)_socket;
}

void socket_transport::setting_transport(void * arg)
{
	_client_socket = (SOCKET)arg;
}

int socket_transport::send_request_server()
{
	std::string url = get_url_and_port(_request);

	//создаем сокет для соеднинения с сервером
	socket_transport socket_serv(url.c_str(), "HTTP", NULL);
	socket_serv.start_connection_server();
	_server_socket = (SOCKET)socket_serv.get_transport();
	my_logger->_INFO_("created socket for server successfully!", TRUE);
	int total_bytes = send_request(_count_bytes, 1);
	
	return total_bytes;
}




