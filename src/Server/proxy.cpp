#include "stdafx.h"
#include "proxy.h"
#include "package.h"

proxy::proxy(void * arg)
{
	_transport = (transport*)arg;
}

void proxy::client_server_communication(package client_package, Statistic & statistic)
{
	std::string request = _transport->get_request_from_client();
	client_package.request = request;
	_transport->send_request_server();
	statistic.translator(client_package);
	static std::string req="";
	
	std::thread thread_send_answer([&]()
	{
		req = _transport->send_answer_to_client();
	});

	client_package.request = req;
	statistic.translator(client_package);
	thread_send_answer.join();
	//statistic->translator(package_client);
}

