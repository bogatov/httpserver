#include "stdafx.h"
#include "get_transport.h"
#include "proxy.h"
#include "server.h"
#include <future>
#include <numeric>
#include "statistic.h"

sockaddr_in client_info{};
socklen_t size_client_info = sizeof(client_info);

void server_socket::start()
{
	std::string ip_adr;
	std::string port;
	set_config(ip_adr, port);

	//создаем транспорт
	transport* _transport = get_transport(Socket, ip_adr.data(), port.data());
	_transport->start_connection_client();
	SOCKET listen_socket = (SOCKET)_transport->get_transport();
	Statistic statistic = Statistic();

	while (true)
	{
		//получаем очередного клиента
		SOCKET client_socket = accept(listen_socket, (sockaddr*)&client_info, &size_client_info);
		package* package_client = new package();
		package_client->int_ip_client = client_info.sin_addr.S_un.S_addr;
		package_client->ip_client = inet_ntoa(client_info.sin_addr);
		std::auto_ptr<socket_transport>transport(new socket_transport(ip_adr.data(), port.data(), client_socket));
		proxy proxy(transport.get());
		std::thread thread_comm_client_server(&proxy::client_server_communication, proxy, *package_client, std::ref(statistic));
		thread_comm_client_server.join();
	}
}

server* create_server(transport_type transport_type)
{
	server* server = NULL;

	switch (transport_type)
	{
		case Socket:
		{
			server = new server_socket();

			break;
		}

		default:
			break;
	}

	return server;
}