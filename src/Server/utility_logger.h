#pragma once
#include <iostream>

long get_file_size(std::string path);
const std::string currentDateTime();
void set_min_level_msg(std::string level_msg, int& min);
void set_max_level_msg(std::string level_msg, int& max);