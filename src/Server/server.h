#pragma once
#include "config.h"

struct server
{
public:
	virtual void start() = 0;
};

//������ - ������� ����������� ��������
struct server_socket : public server
{
public:
	server_socket() {};
	void start() override;
};

//������ - ������� ������ ��� �������� ��������
server* create_server(transport_type transport_type);